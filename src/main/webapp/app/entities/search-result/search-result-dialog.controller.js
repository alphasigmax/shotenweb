(function() {
    'use strict';

    angular
        .module('shotenwebApp')
        .controller('SearchResultDialogController', SearchResultDialogController);

    SearchResultDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'SearchResult'];

    function SearchResultDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, SearchResult) {
        var vm = this;

        vm.searchResult = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.searchResult.id !== null) {
                SearchResult.update(vm.searchResult, onSaveSuccess, onSaveError);
            } else {
                SearchResult.save(vm.searchResult, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('shotenwebApp:searchResultUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();

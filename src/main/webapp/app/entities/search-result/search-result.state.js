(function() {
    'use strict';

    angular
        .module('shotenwebApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('search-result', {
            parent: 'entity',
            url: '/search-result?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'shotenwebApp.searchResult.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/search-result/search-results.html',
                    controller: 'SearchResultController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('searchResult');
                    $translatePartialLoader.addPart('searchType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('search-result-detail', {
            parent: 'entity',
            url: '/search-result/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'shotenwebApp.searchResult.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/search-result/search-result-detail.html',
                    controller: 'SearchResultDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('searchResult');
                    $translatePartialLoader.addPart('searchType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'SearchResult', function($stateParams, SearchResult) {
                    return SearchResult.get({id : $stateParams.id}).$promise;
                }]
            }
        })
        .state('search-result.new', {
            parent: 'search-result',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/search-result/search-result-dialog.html',
                    controller: 'SearchResultDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                title: null,
                                url: null,
                                searchType: null,
                                snippet: null,
                                thumbnail: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('search-result', null, { reload: true });
                }, function() {
                    $state.go('search-result');
                });
            }]
        })
        .state('search-result.edit', {
            parent: 'search-result',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/search-result/search-result-dialog.html',
                    controller: 'SearchResultDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['SearchResult', function(SearchResult) {
                            return SearchResult.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('search-result', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('search-result.delete', {
            parent: 'search-result',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/search-result/search-result-delete-dialog.html',
                    controller: 'SearchResultDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['SearchResult', function(SearchResult) {
                            return SearchResult.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('search-result', null, { reload: true });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();

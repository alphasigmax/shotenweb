(function() {
    'use strict';

    angular
        .module('shotenwebApp')
        .controller('SearchResultDeleteController',SearchResultDeleteController);

    SearchResultDeleteController.$inject = ['$uibModalInstance', 'entity', 'SearchResult'];

    function SearchResultDeleteController($uibModalInstance, entity, SearchResult) {
        var vm = this;

        vm.searchResult = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;
        
        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            SearchResult.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();

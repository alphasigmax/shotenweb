(function() {
    'use strict';

    angular
        .module('shotenwebApp')
        .controller('SearchResultDetailController', SearchResultDetailController);

    SearchResultDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'entity', 'SearchResult'];

    function SearchResultDetailController($scope, $rootScope, $stateParams, entity, SearchResult) {
        var vm = this;

        vm.searchResult = entity;

        var unsubscribe = $rootScope.$on('shotenwebApp:searchResultUpdate', function(event, result) {
            vm.searchResult = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();

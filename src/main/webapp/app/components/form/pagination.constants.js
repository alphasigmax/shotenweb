(function() {
    'use strict';

    angular
        .module('shotenwebApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();

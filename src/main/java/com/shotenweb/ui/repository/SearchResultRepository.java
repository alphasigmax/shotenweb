package com.shotenweb.ui.repository;

import com.shotenweb.ui.domain.SearchResult;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the SearchResult entity.
 */
@SuppressWarnings("unused")
public interface SearchResultRepository extends JpaRepository<SearchResult,Long> {

}

package com.shotenweb.ui.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.shotenweb.ui.domain.SearchResult;
import com.shotenweb.ui.repository.SearchResultRepository;
import com.shotenweb.ui.web.rest.util.HeaderUtil;
import com.shotenweb.ui.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SearchResult.
 */
@RestController
@RequestMapping("/api")
public class SearchResultResource {

    private final Logger log = LoggerFactory.getLogger(SearchResultResource.class);
        
    @Inject
    private SearchResultRepository searchResultRepository;
    
    /**
     * POST  /search-results : Create a new searchResult.
     *
     * @param searchResult the searchResult to create
     * @return the ResponseEntity with status 201 (Created) and with body the new searchResult, or with status 400 (Bad Request) if the searchResult has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/search-results",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SearchResult> createSearchResult(@RequestBody SearchResult searchResult) throws URISyntaxException {
        log.debug("REST request to save SearchResult : {}", searchResult);
        if (searchResult.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert("searchResult", "idexists", "A new searchResult cannot already have an ID")).body(null);
        }
        SearchResult result = searchResultRepository.save(searchResult);
        return ResponseEntity.created(new URI("/api/search-results/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert("searchResult", result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /search-results : Updates an existing searchResult.
     *
     * @param searchResult the searchResult to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated searchResult,
     * or with status 400 (Bad Request) if the searchResult is not valid,
     * or with status 500 (Internal Server Error) if the searchResult couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @RequestMapping(value = "/search-results",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SearchResult> updateSearchResult(@RequestBody SearchResult searchResult) throws URISyntaxException {
        log.debug("REST request to update SearchResult : {}", searchResult);
        if (searchResult.getId() == null) {
            return createSearchResult(searchResult);
        }
        SearchResult result = searchResultRepository.save(searchResult);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert("searchResult", searchResult.getId().toString()))
            .body(result);
    }

    /**
     * GET  /search-results : get all the searchResults.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of searchResults in body
     * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
     */
    @RequestMapping(value = "/search-results",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<List<SearchResult>> getAllSearchResults(Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of SearchResults");
        Page<SearchResult> page = searchResultRepository.findAll(pageable); 
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/search-results");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /search-results/:id : get the "id" searchResult.
     *
     * @param id the id of the searchResult to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the searchResult, or with status 404 (Not Found)
     */
    @RequestMapping(value = "/search-results/{id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SearchResult> getSearchResult(@PathVariable Long id) {
        log.debug("REST request to get SearchResult : {}", id);
        SearchResult searchResult = searchResultRepository.findOne(id);
        return Optional.ofNullable(searchResult)
            .map(result -> new ResponseEntity<>(
                result,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /search-results/:id : delete the "id" searchResult.
     *
     * @param id the id of the searchResult to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @RequestMapping(value = "/search-results/{id}",
        method = RequestMethod.DELETE,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> deleteSearchResult(@PathVariable Long id) {
        log.debug("REST request to delete SearchResult : {}", id);
        searchResultRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("searchResult", id.toString())).build();
    }

}

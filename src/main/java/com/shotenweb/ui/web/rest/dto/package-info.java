/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.shotenweb.ui.web.rest.dto;

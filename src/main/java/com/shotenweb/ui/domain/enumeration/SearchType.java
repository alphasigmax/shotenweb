package com.shotenweb.ui.domain.enumeration;

/**
 * The SearchType enumeration.
 */
public enum SearchType {
    web,image,news,shopping,video
}

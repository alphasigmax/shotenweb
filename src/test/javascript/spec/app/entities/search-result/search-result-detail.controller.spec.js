'use strict';

describe('Controller Tests', function() {

    describe('SearchResult Management Detail Controller', function() {
        var $scope, $rootScope;
        var MockEntity, MockSearchResult;
        var createController;

        beforeEach(inject(function($injector) {
            $rootScope = $injector.get('$rootScope');
            $scope = $rootScope.$new();
            MockEntity = jasmine.createSpy('MockEntity');
            MockSearchResult = jasmine.createSpy('MockSearchResult');
            

            var locals = {
                '$scope': $scope,
                '$rootScope': $rootScope,
                'entity': MockEntity ,
                'SearchResult': MockSearchResult
            };
            createController = function() {
                $injector.get('$controller')("SearchResultDetailController", locals);
            };
        }));


        describe('Root Scope Listening', function() {
            it('Unregisters root scope listener upon scope destruction', function() {
                var eventType = 'shotenwebApp:searchResultUpdate';

                createController();
                expect($rootScope.$$listenerCount[eventType]).toEqual(1);

                $scope.$destroy();
                expect($rootScope.$$listenerCount[eventType]).toBeUndefined();
            });
        });
    });

});

package com.shotenweb.ui.web.rest;

import com.shotenweb.ui.ShotenwebApp;
import com.shotenweb.ui.domain.SearchResult;
import com.shotenweb.ui.repository.SearchResultRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.shotenweb.ui.domain.enumeration.SearchType;

/**
 * Test class for the SearchResultResource REST controller.
 *
 * @see SearchResultResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ShotenwebApp.class)
@WebAppConfiguration
@IntegrationTest
public class SearchResultResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAA";
    private static final String UPDATED_TITLE = "BBBBB";
    private static final String DEFAULT_URL = "AAAAA";
    private static final String UPDATED_URL = "BBBBB";

    private static final SearchType DEFAULT_SEARCH_TYPE = SearchType.web;
    private static final SearchType UPDATED_SEARCH_TYPE = SearchType.image;
    private static final String DEFAULT_SNIPPET = "AAAAA";
    private static final String UPDATED_SNIPPET = "BBBBB";
    private static final String DEFAULT_THUMBNAIL = "AAAAA";
    private static final String UPDATED_THUMBNAIL = "BBBBB";

    @Inject
    private SearchResultRepository searchResultRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Inject
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restSearchResultMockMvc;

    private SearchResult searchResult;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SearchResultResource searchResultResource = new SearchResultResource();
        ReflectionTestUtils.setField(searchResultResource, "searchResultRepository", searchResultRepository);
        this.restSearchResultMockMvc = MockMvcBuilders.standaloneSetup(searchResultResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        searchResult = new SearchResult();
        searchResult.setTitle(DEFAULT_TITLE);
        searchResult.setUrl(DEFAULT_URL);
        searchResult.setSearchType(DEFAULT_SEARCH_TYPE);
        searchResult.setSnippet(DEFAULT_SNIPPET);
        searchResult.setThumbnail(DEFAULT_THUMBNAIL);
    }

    @Test
    @Transactional
    public void createSearchResult() throws Exception {
        int databaseSizeBeforeCreate = searchResultRepository.findAll().size();

        // Create the SearchResult

        restSearchResultMockMvc.perform(post("/api/search-results")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(searchResult)))
                .andExpect(status().isCreated());

        // Validate the SearchResult in the database
        List<SearchResult> searchResults = searchResultRepository.findAll();
        assertThat(searchResults).hasSize(databaseSizeBeforeCreate + 1);
        SearchResult testSearchResult = searchResults.get(searchResults.size() - 1);
        assertThat(testSearchResult.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testSearchResult.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testSearchResult.getSearchType()).isEqualTo(DEFAULT_SEARCH_TYPE);
        assertThat(testSearchResult.getSnippet()).isEqualTo(DEFAULT_SNIPPET);
        assertThat(testSearchResult.getThumbnail()).isEqualTo(DEFAULT_THUMBNAIL);
    }

    @Test
    @Transactional
    public void getAllSearchResults() throws Exception {
        // Initialize the database
        searchResultRepository.saveAndFlush(searchResult);

        // Get all the searchResults
        restSearchResultMockMvc.perform(get("/api/search-results?sort=id,desc"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(searchResult.getId().intValue())))
                .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
                .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL.toString())))
                .andExpect(jsonPath("$.[*].searchType").value(hasItem(DEFAULT_SEARCH_TYPE.toString())))
                .andExpect(jsonPath("$.[*].snippet").value(hasItem(DEFAULT_SNIPPET.toString())))
                .andExpect(jsonPath("$.[*].thumbnail").value(hasItem(DEFAULT_THUMBNAIL.toString())));
    }

    @Test
    @Transactional
    public void getSearchResult() throws Exception {
        // Initialize the database
        searchResultRepository.saveAndFlush(searchResult);

        // Get the searchResult
        restSearchResultMockMvc.perform(get("/api/search-results/{id}", searchResult.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(searchResult.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL.toString()))
            .andExpect(jsonPath("$.searchType").value(DEFAULT_SEARCH_TYPE.toString()))
            .andExpect(jsonPath("$.snippet").value(DEFAULT_SNIPPET.toString()))
            .andExpect(jsonPath("$.thumbnail").value(DEFAULT_THUMBNAIL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingSearchResult() throws Exception {
        // Get the searchResult
        restSearchResultMockMvc.perform(get("/api/search-results/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSearchResult() throws Exception {
        // Initialize the database
        searchResultRepository.saveAndFlush(searchResult);
        int databaseSizeBeforeUpdate = searchResultRepository.findAll().size();

        // Update the searchResult
        SearchResult updatedSearchResult = new SearchResult();
        updatedSearchResult.setId(searchResult.getId());
        updatedSearchResult.setTitle(UPDATED_TITLE);
        updatedSearchResult.setUrl(UPDATED_URL);
        updatedSearchResult.setSearchType(UPDATED_SEARCH_TYPE);
        updatedSearchResult.setSnippet(UPDATED_SNIPPET);
        updatedSearchResult.setThumbnail(UPDATED_THUMBNAIL);

        restSearchResultMockMvc.perform(put("/api/search-results")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(updatedSearchResult)))
                .andExpect(status().isOk());

        // Validate the SearchResult in the database
        List<SearchResult> searchResults = searchResultRepository.findAll();
        assertThat(searchResults).hasSize(databaseSizeBeforeUpdate);
        SearchResult testSearchResult = searchResults.get(searchResults.size() - 1);
        assertThat(testSearchResult.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testSearchResult.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testSearchResult.getSearchType()).isEqualTo(UPDATED_SEARCH_TYPE);
        assertThat(testSearchResult.getSnippet()).isEqualTo(UPDATED_SNIPPET);
        assertThat(testSearchResult.getThumbnail()).isEqualTo(UPDATED_THUMBNAIL);
    }

    @Test
    @Transactional
    public void deleteSearchResult() throws Exception {
        // Initialize the database
        searchResultRepository.saveAndFlush(searchResult);
        int databaseSizeBeforeDelete = searchResultRepository.findAll().size();

        // Get the searchResult
        restSearchResultMockMvc.perform(delete("/api/search-results/{id}", searchResult.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<SearchResult> searchResults = searchResultRepository.findAll();
        assertThat(searchResults).hasSize(databaseSizeBeforeDelete - 1);
    }
}
